WLAN=wlp3s0
OUT_INTERFACE=enp0s25

start: forward unblock-radio iptables hostapd ip-address dhcp-server

stop:
	sudo pkill udhcpd || true
	sudo pkill hostapd || true
	sudo ip addr del 10.10.10.1/24 dev $(WLAN) || true

forward:
	sudo bash -c 'echo 1 > /proc/sys/net/ipv4/ip_forward'

unblock-radio:
	sudo nmcli radio wifi off
	sudo rfkill unblock wifi

iptables:
	sudo iptables -t nat -A POSTROUTING -o $(OUT_INTERFACE) -j MASQUERADE
	sudo iptables -A FORWARD -i $(OUT_INTERFACE) -o $(WLAN) -m state --state RELATED,ESTABLISHED -j ACCEPT
	sudo iptables -A FORWARD -i $(WLAN) -o $(OUT_INTERFACE) -j ACCEPT

hostapd:
	sudo hostapd -B hostapd.conf

ip-address:
	sudo ip addr add 10.10.10.1/24 dev $(WLAN)

dhcp-server:
	sudo udhcpd udhcpd.conf
